MCForge Classic
===============

Minecraft Classic Custom Server Software


About MCForge Classic
---------------------

MCForge classic is a classic Minecraft server software based on MCLawl.  It has been released under an open-source license to allow the community to help us develop it further.  We welcome all pull requests and will merge them in at our own discretion.

The official website for MCForge is [http://forgewareinc.com/mcforge.html][2]

Contributors
------------

* If you are pushing your commits OR if you are doing a pull request, PLEASE make sure to NORMALIZE your entire local repo if it isn't already normalized. The head repo is already normalized, so if you forked or pulled from the HEAD, everything should be alright. See [this link][4] for more info on re-normalizing your repo.  
**!NOTE!** Commits that do not handle the line-endings correctly *WILL* be reverted.  
  
* Do not commit large blocks of changes and files. Try to split and group these into smaller related pieces and commit these one by one. If you are adding a new file that might have 100+ lines of code additions, then that's allowed, just don't add anything else unrelated to that commit!
  
* Please make sure you fetch and merge the latest commits into your local branch before pushing any commits where you might have changed files that would cause a conflict. This does **NOT** mean that you have to fetch and merge with *every* commitment or push. Just keep track of the latest commitments and see if someone else might have changed a file you've been working on.  
A good habbit would be to *first* make some couple of commitments, then BEFORE pushing, do a fetch/merge and fix any conflicts (if there are any).

Copyright/License
-----------------
    MCForge Server SoftWare For Minecraft Classic
    Copyright (C) 2011-2017  David Mitchell

    Dual-licensed under the	Educational Community License, Version 2.0 and
	the GNU General Public License, Version 3 (the "Licenses"); you may
	not use this file except in compliance with the Licenses. You may
	obtain a copy of the Licenses at
	
	http://www.opensource.org/licenses/ecl2.php
	http://www.gnu.org/licenses/gpl-3.0.html
	
	Unless required by applicable law or agreed to in writing,
	software distributed under the Licenses are distributed on an "AS IS"
	BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
	or implied. See the Licenses for the specific language governing
	permissions and limitations under the Licenses.

    In an effort to add copyright notices to the source code, we utilized automated methods to insert these. Some copyright attributions may be incorrect. If you identify any such cases, please notify us immediately so we may correct it.
    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
    It is also required that any derivative works clearly state that they are based off of MCForge Scorce code.